import React, { useState, useEffect } from 'react'
import { motion } from "framer-motion";
import Title from './Title'
const Page2 = () => {
    const imageAnimate = {
        offscreen: {
            y: 100,
            opacity: 0
        },
        onscreen: {
            y: 0,
            opacity: 1,
            transition: {
                type: "spring",
                bounce: 0.4,
                duration: 0.8
            }
        }
    };
    const [scrolled, setScrolled] = useState(0);
    useEffect(() => {
        window.addEventListener('scroll', scrollProgress);
        return () => {
            window.removeEventListener('scroll', scrollProgress);
        }
    }, []);
    const scrollProgress = () => {
        const scrollPx = document.documentElement.scrollTop;
        console.log(`scrollPX: ${scrollPx}`);
        const winHeightPx = document.documentElement.scrollHeight - document.documentElement.clientHeight;
        console.log(`winHeightPx: ${winHeightPx}`);
        const scrolled = Math.ceil(scrollPx / winHeightPx * 100 / 0.7);
        console.log(`scrolled: ${scrolled}`);
        console.log(`scrolledPad: ${scrolled.toString().padStart(4, '0')}`);
        setScrolled(scrolled);
    };

    const img = `https://www.apple.com/105/media/us/airpods-pro/2019/1299e2f5_9206_4470_b28e_08307a42f19b/anim/sequence/large/01-hero-lightpass/${scrolled.toString().padStart(4, '0')}.jpg`
    return (
        <motion.div className="firstScreen"

            initial="offscreen"
            whileInView="onscreen"

            transition={{
                staggerChildren: 0.5,
                delayChildren: 0.5,
                type: "spring",
                bounce: 0.4,

                duration: 0.5,
                ease: "easeInOut"

            }}
            variants={imageAnimate}
        >
            <div
                style={{
                    height: "100vh",
                    position: "sticky",
                    top: 0,
                    backgroundColor: "black",
                }}
                className="sticky"
            >
                <img
                    id='spinner'
                    src={img} alt={""}
                    style={{
                       
                        width: "100%",
                        height: "100%",

                        objectFit: "contain",
                        position: "absolute",
                        top: 0,
                        zIndex: -1,
                        left: 0,
                    }}
                />

                <Title />
            </div>
        </motion.div>
    )
}

export default Page2